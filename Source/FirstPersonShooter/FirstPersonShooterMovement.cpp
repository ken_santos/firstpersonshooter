// Fill out your copyright notice in the Description page of Project Settings.


#include "FirstPersonShooterMovement.h"
#include "GameFramework/Character.h"

UFirstPersonShooterMovement::UFirstPersonShooterMovement(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
	SprintSpeedMultiplier = 2.0f;
	SprintAccelerationMultiplier = 2.0f;
}

void UFirstPersonShooterMovement::SetSprinting(bool bSprinting)
{
	bWantsToSprint = bSprinting;
}

float UFirstPersonShooterMovement::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	if (bWantsToSprint)
	{
		MaxSpeed *= SprintSpeedMultiplier;
	}

	return MaxSpeed;
}

float UFirstPersonShooterMovement::GetMaxAcceleration() const
{
	float MaxAccel = Super::GetMaxAcceleration();

	if (bWantsToSprint)
	{
		MaxAccel *= SprintAccelerationMultiplier;
	}

	return MaxAccel;
}

//Set input flags on character from saved inputs
void UFirstPersonShooterMovement::UpdateFromCompressedFlags(uint8 Flags)//Client only
{
	Super::UpdateFromCompressedFlags(Flags);

	//The Flags parameter contains the compressed input flags that are stored in the saved move.
	//UpdateFromCompressed flags simply copies the flags from the saved move into the movement component.
	//It basically just resets the movement component to the state when the move was made so it can simulate from there.
	bWantsToSprint = (Flags & FSavedMove_Character::FLAG_Custom_0) != 0;

}

class FNetworkPredictionData_Client* UFirstPersonShooterMovement::GetPredictionData_Client() const
{
	check(PawnOwner != NULL);
	check(PawnOwner->GetRemoteRole() < ROLE_Authority);

	if (!ClientPredictionData)
	{
		UFirstPersonShooterMovement* MutableThis = const_cast<UFirstPersonShooterMovement*>(this);

		MutableThis->ClientPredictionData = new FNetworkPredictionData_Client_FirstPersonShooterCharacter(*this);
		MutableThis->ClientPredictionData->MaxSmoothNetUpdateDist = 92.f;
		MutableThis->ClientPredictionData->NoSmoothNetUpdateDist = 140.f;
	}

	return ClientPredictionData;
}

void FSavedMove_FirstPersonShooterMovement::Clear()
{
	Super::Clear();

	//Clear variables back to their default values.
	bSavedWantsToSprint = false;
}

uint8 FSavedMove_FirstPersonShooterMovement::GetCompressedFlags() const
{
	uint8 Result = Super::GetCompressedFlags();

	if (bSavedWantsToSprint)
	{
		Result |= FLAG_Custom_0;
	}

	return Result;
}

bool FSavedMove_FirstPersonShooterMovement::CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* Character, float MaxDelta) const
{
	//This pretty much just tells the engine if it can optimize by combining saved moves. There doesn't appear to be
	//any problem with leaving it out, but it seems that it's good practice to implement this anyways.
	if (bSavedWantsToSprint != ((FSavedMove_FirstPersonShooterMovement*)&NewMove)->bSavedWantsToSprint)
	{
		return false;
	}

	return Super::CanCombineWith(NewMove, Character, MaxDelta);
}

void FSavedMove_FirstPersonShooterMovement::SetMoveFor(ACharacter* Character, float InDeltaTime, FVector const& NewAccel, class FNetworkPredictionData_Client_Character& ClientData)
{
	Super::SetMoveFor(Character, InDeltaTime, NewAccel, ClientData);

	UFirstPersonShooterMovement* CharMov = Cast<UFirstPersonShooterMovement>(Character->GetCharacterMovement());
	if (CharMov)
	{
		//This is literally just the exact opposite of UpdateFromCompressed flags. We're taking the input
		//from the player and storing it in the saved move.
		bSavedWantsToSprint = CharMov->bWantsToSprint;
	}
}

void FSavedMove_FirstPersonShooterMovement::PrepMoveFor(class ACharacter* Character)
{
	Super::PrepMoveFor(Character);

	UFirstPersonShooterMovement* CharMov = Cast<UFirstPersonShooterMovement>(Character->GetCharacterMovement());
	if (CharMov)
	{

	}
}

FNetworkPredictionData_Client_FirstPersonShooterCharacter::FNetworkPredictionData_Client_FirstPersonShooterCharacter(const UCharacterMovementComponent& ClientMovement)
	: Super(ClientMovement)
{

}

FSavedMovePtr FNetworkPredictionData_Client_FirstPersonShooterCharacter::AllocateNewMove()
{
	return FSavedMovePtr(new FSavedMove_FirstPersonShooterMovement());
}