// Fill out your copyright notice in the Description page of Project Settings.


#include "FirstPersonGameState.h"
#include "Kismet/KismetSystemLibrary.h"

void AFirstPersonGameState::TriggerAOEEvent(const FVector& Location, float Radius, bool bRequireLineOfSight,  AActor* const& AOESource, FOnAOEHitDelegate Callback, float DrawDebugDuration, const FLinearColor& DrawDebugColor)
{
	OnAOEHitDelegate = Callback;

	if (DrawDebugDuration > 0.0f) {
		UKismetSystemLibrary::DrawDebugSphere(GetWorld(), Location, Radius, 16, DrawDebugColor, DrawDebugDuration, 2.0f);

		TArray<TEnumAsByte<EObjectTypeQuery>> traceObjectTypes;
		traceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));

		UClass* seekClass = AActor::StaticClass();

		TArray<AActor*> actorsToIgnore;
		actorsToIgnore.Init(AOESource, 1);

		TArray<AActor*> outActors;

		UKismetSystemLibrary::SphereOverlapActors(GetWorld(), Location, Radius, traceObjectTypes, seekClass, actorsToIgnore, outActors);

		for (AActor* overlappedActor : outActors) {
			OnAOEHitDelegate.Broadcast(overlappedActor);
		}
	}
}