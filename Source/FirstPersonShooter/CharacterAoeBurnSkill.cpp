// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterAoeBurnSkill.h"
#include "FirstPersonGameState.h"
#include "Kismet/KismetSystemLibrary.h"
#include "StatusEffectComponent.h"
#include "GameFramework/Character.h"

void UCharacterAoeBurnSkill::ActivateSkill()
{
	Super::ActivateSkill();

	// Spawn Visual indicator for AOE reach
	SpawnAOESphere(GetOwner()->GetActorLocation(), AoeRadius, 2.5f, AoeColor);
	
	// Checking of ENemy hit on the server
	ServerTriggerAOESphere(GetOwner()->GetActorLocation(), AoeRadius);
}

void UCharacterAoeBurnSkill::ApplyBurnStatusEffect(AActor* TargetEnemy)
{
	UStatusEffectComponent* BurnStatusEffectComponent;
	if (BurnStatusEffectClass)
	{
		FName StatusEffectName("BurnStatusEffectComponent");
		BurnStatusEffectComponent = NewObject<UStatusEffectComponent>(TargetEnemy, BurnStatusEffectClass, StatusEffectName);

		if (BurnStatusEffectComponent) {
			BurnStatusEffectComponent->RegisterComponent();
			BurnStatusEffectComponent->AttachToComponent(TargetEnemy->GetRootComponent(), FAttachmentTransformRules::SnapToTargetIncludingScale, NAME_None);
			TargetEnemy->AddInstanceComponent(BurnStatusEffectComponent);
			
			BurnStatusEffectComponent->SetDamageSource(GetOwner());
			BurnStatusEffectComponent->ApplyStatusEffect();
		}
	}
}

void UCharacterAoeBurnSkill::SpawnAOESphere(const FVector& Location, float Radius, float DrawDebugDuration, const FLinearColor& DrawDebugColor)
{
	if (DrawDebugDuration > 0.0f) {
		UKismetSystemLibrary::DrawDebugSphere(GetWorld(), Location, Radius, 16, DrawDebugColor, DrawDebugDuration, 2.0f);
	}
}

void UCharacterAoeBurnSkill::ServerTriggerAOESphere_Implementation(const FVector& Location, float Radius)
{
	// Object types to check
	TArray<TEnumAsByte<EObjectTypeQuery>> traceObjectTypes;
	traceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));

	UClass* seekClass = ACharacter::StaticClass();

	// Ignore caster
	TArray<AActor*> actorsToIgnore;
	actorsToIgnore.Init(GetOwner(), 1);

	TArray<AActor*> outActors;

	UKismetSystemLibrary::SphereOverlapActors(GetWorld(), Location, Radius, traceObjectTypes, seekClass, actorsToIgnore, outActors);

	for (AActor* overlappedActor : outActors) {
		// Add Replicated Component
		ApplyBurnStatusEffect(overlappedActor);
	}
}
