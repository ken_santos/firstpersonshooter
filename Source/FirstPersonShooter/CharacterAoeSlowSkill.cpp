// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterAoeSlowSkill.h"
#include "FirstPersonGameState.h"
#include "Kismet/KismetSystemLibrary.h"
#include "StatusEffectComponent.h"
#include "GameFramework/Character.h"

void UCharacterAoeSlowSkill::ActivateSkill()
{
	Super::ActivateSkill();

	// Spawn Visual indicator for AOE reach
	SpawnAOESphere(GetOwner()->GetActorLocation(), AoeRadius, 2.5f, AoeColor);

	// Checking of ENemy hit on the server
	ServerTriggerAOESphere(GetOwner()->GetActorLocation(), AoeRadius);
}

void UCharacterAoeSlowSkill::ApplySlowStatusEffect(AActor* TargetEnemy)
{
	UStatusEffectComponent* SlowStatusEffectComponent;
	if (SlowStatusEffectClass)
	{
		FName StatusEffectName("SlowStatusEffectComponent");
		SlowStatusEffectComponent = NewObject<UStatusEffectComponent>(TargetEnemy, SlowStatusEffectClass, StatusEffectName);

		if (SlowStatusEffectComponent) {
			SlowStatusEffectComponent->RegisterComponent();
			SlowStatusEffectComponent->AttachToComponent(TargetEnemy->GetRootComponent(), FAttachmentTransformRules::SnapToTargetIncludingScale, NAME_None);
			TargetEnemy->AddInstanceComponent(SlowStatusEffectComponent);

			SlowStatusEffectComponent->SetDamageSource(GetOwner());
			SlowStatusEffectComponent->ApplyStatusEffect();
		}
	}
}

void UCharacterAoeSlowSkill::SpawnAOESphere(const FVector& Location, float Radius, float DrawDebugDuration, const FLinearColor& DrawDebugColor)
{
	if (DrawDebugDuration > 0.0f) {
		UKismetSystemLibrary::DrawDebugSphere(GetWorld(), Location, Radius, 16, DrawDebugColor, DrawDebugDuration, 2.0f);
	}
}

void UCharacterAoeSlowSkill::ServerTriggerAOESphere_Implementation(const FVector& Location, float Radius)
{
	// Object types to check
	TArray<TEnumAsByte<EObjectTypeQuery>> traceObjectTypes;
	traceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));

	UClass* seekClass = ACharacter::StaticClass();

	// Ignore caster
	TArray<AActor*> actorsToIgnore;
	actorsToIgnore.Init(GetOwner(), 1);

	TArray<AActor*> outActors;

	UKismetSystemLibrary::SphereOverlapActors(GetWorld(), Location, Radius, traceObjectTypes, seekClass, actorsToIgnore, outActors);

	for (AActor* overlappedActor : outActors) {
		// Add Replicated Component
		ApplySlowStatusEffect(overlappedActor);
	}
}

