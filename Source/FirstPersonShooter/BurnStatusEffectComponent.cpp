// Fill out your copyright notice in the Description page of Project Settings.


#include "BurnStatusEffectComponent.h"
#include "FirstPersonShooterCharacter.h"

void UBurnStatusEffectComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UBurnStatusEffectComponent::ApplyStatusEffect()
{
	ServerDamageTimer();
}

void UBurnStatusEffectComponent::ServerDamageTimer_Implementation()
{
	AFirstPersonShooterCharacter* owner = Cast<AFirstPersonShooterCharacter>(GetOwner());

	if (owner) {
		FTimerHandle Timer;
		GetOwner()->GetWorldTimerManager().SetTimer(Timer, this, &UBurnStatusEffectComponent::DamageOwner, DamageRateInSeconds, true);
	}
}

void UBurnStatusEffectComponent::DamageOwner()
{
	
}