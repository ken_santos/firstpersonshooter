// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CharacterBaseSkill.h"
#include "CharacterSuperSkill.generated.h"

/**
 * 
 */
UCLASS()
class FIRSTPERSONSHOOTER_API UCharacterSuperSkill : public UCharacterBaseSkill
{
	GENERATED_BODY()
	
public:
	// Used for Activating the Skill
	virtual void ActivateSkill() override;
};
