// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StatusEffectComponent.h"
#include "SlowStatusEffectComponent.generated.h"

/**
 * 
 */
UCLASS()
class FIRSTPERSONSHOOTER_API USlowStatusEffectComponent : public UStatusEffectComponent
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditDefaultsOnly, Category = General)
	float SlowMultiplier = 5.0f;

	virtual void BeginPlay() override;

	virtual void ApplyStatusEffect() override;

	UFUNCTION(Server, Reliable)
	void ServerApplySlow();

	UFUNCTION()
	void StopSlow();
};
