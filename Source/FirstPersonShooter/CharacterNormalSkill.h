// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CharacterBaseSkill.h"
#include "CharacterNormalSkill.generated.h"

/**
 * 
 */
UCLASS()
class FIRSTPERSONSHOOTER_API UCharacterNormalSkill : public UCharacterBaseSkill
{
	GENERATED_BODY()
	
public:
	// Used for Activating the Skill
	virtual void ActivateSkill() override;
};
