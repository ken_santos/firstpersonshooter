// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FloatingTextWidget.generated.h"

/**
 * 
 */
UCLASS()
class FIRSTPERSONSHOOTER_API UFloatingTextWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void PlayFloatingText(float Amount);

protected:
	virtual void NativeConstruct() override;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* FloatingTextBlock;
	
	class UWidgetAnimation* GetAnimationByName(FName AnimationName) const;

	void StoreWidgetAnimations();

private:
	TMap<FName, UWidgetAnimation*> AnimationsMap;

	class UWidgetAnimation* FloatingTextAnimation;
};
