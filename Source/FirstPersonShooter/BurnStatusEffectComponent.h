// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StatusEffectComponent.h"
#include "BurnStatusEffectComponent.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class FIRSTPERSONSHOOTER_API UBurnStatusEffectComponent : public UStatusEffectComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, Category = General)
	float DamageRateInSeconds = 1.0f;

	virtual void BeginPlay() override;

	virtual void ApplyStatusEffect() override;

	UFUNCTION(Server, Reliable)
	void ServerDamageTimer();

	void DamageOwner();

};
