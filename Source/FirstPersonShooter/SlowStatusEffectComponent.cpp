// Fill out your copyright notice in the Description page of Project Settings.


#include "SlowStatusEffectComponent.h"
#include "FirstPersonShooterCharacter.h"

void USlowStatusEffectComponent::BeginPlay()
{
	Super::BeginPlay();
}

void USlowStatusEffectComponent::ApplyStatusEffect()
{
	ServerApplySlow();
}

void USlowStatusEffectComponent::ServerApplySlow_Implementation()
{
	AFirstPersonShooterCharacter* owner = Cast<AFirstPersonShooterCharacter>(GetOwner());

	if (owner) {
		owner->StartSlow(SlowMultiplier);

		FTimerHandle Timer;
		GetOwner()->GetWorldTimerManager().SetTimer(Timer, this, &USlowStatusEffectComponent::StopSlow, StatusEffectDuration - 0.05f, false);
	}
}

void USlowStatusEffectComponent::StopSlow()
{
	AFirstPersonShooterCharacter* owner = Cast<AFirstPersonShooterCharacter>(GetOwner());

	if (owner) {
		owner->StopSlow(SlowMultiplier);
	}

}