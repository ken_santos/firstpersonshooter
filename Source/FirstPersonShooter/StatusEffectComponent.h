// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Particles/ParticleSystemComponent.h"
#include "StatusEffectComponent.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class FIRSTPERSONSHOOTER_API UStatusEffectComponent : public UParticleSystemComponent
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;
	
	UPROPERTY(EditDefaultsOnly, Category = General)
	float StatusEffectDuration = 1.0f;

	UFUNCTION()
	virtual void ApplyStatusEffect();
	
	void SetDamageSource(AActor* EffectDamageSource);

protected:
	// Caster of the status effect component
	AActor* DamageSource;
	
private:
	UFUNCTION()
	void DestroySelfAfterTimer();

};
