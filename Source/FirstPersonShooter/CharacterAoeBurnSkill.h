// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CharacterSuperSkill.h"
#include "CharacterAoeBurnSkill.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class FIRSTPERSONSHOOTER_API UCharacterAoeBurnSkill : public UCharacterSuperSkill
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, Category = General)
	float AoeRadius = 400.0f;
	
	UPROPERTY(EditDefaultsOnly, Category = General)
	FLinearColor AoeColor = FLinearColor::Red;

	// Used for Activating the Skill
	// AOE Burn Skill: Activates an AOE effect that burns the target caught up in the area overtime
	virtual void ActivateSkill() override;

	// Applying Burn Damage to Enemies
	UFUNCTION()
	void ApplyBurnStatusEffect(AActor* TargetEnemy);

	// Status effect to use on the opponent
	UPROPERTY(EditAnywhere, Category = General)
	TSubclassOf<class UStatusEffectComponent> BurnStatusEffectClass;
	
	// Shows A DebugSpehere for visual purposes
	UFUNCTION()
	void SpawnAOESphere(const FVector& Location, float Radius, float DrawDebugDuration, const FLinearColor& DrawDebugColor);

	// Check if the player in the server has hit anyone with the AOE skill
	// If yes, then apply the Burn Status effect
	UFUNCTION(Server, Reliable)
	void ServerTriggerAOESphere(const FVector& Location, float Radius);


};
