// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "FirstPersonGameState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAOEHitDelegate, AActor*, HitActor);

/**
 * 
 */
UCLASS()
class FIRSTPERSONSHOOTER_API AFirstPersonGameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	UFUNCTION()
	void TriggerAOEEvent(const FVector& Location, float Radius, bool bRequireLineOfSight, AActor* const& AOESource, FOnAOEHitDelegate Callback, float DrawDebugDuration, const FLinearColor& DrawDebugColor);

	UPROPERTY(BlueprintAssignable, Category = AOE)
	FOnAOEHitDelegate OnAOEHitDelegate;
};
