// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Damageable.h"
#include "ImplementsLeftClick.h"
#include "FirstPersonShooterCharacter.generated.h"

class UInputComponent;
class USkeletalMeshComponent;
class USceneComponent;
class UCameraComponent;
class UMotionControllerComponent;
class UAnimMontage;
class USoundBase;
class UWidgetComponent;
class UCharacterSuperSkill;
class UCharacterNormalSkill;

UCLASS(config=Game)
class AFirstPersonShooterCharacter : public ACharacter, public IDamageable
{
	GENERATED_BODY()

	/** The super skill of the character */
	UPROPERTY(VisibleDefaultsOnly, Category = General)
	UCharacterSuperSkill* CharacterSuperSkillComponent;

	/** The normal skill of the character */
	UPROPERTY(VisibleDefaultsOnly, Category = General)
	UCharacterNormalSkill* CharacterNormalSkillComponent;

	/** The widget Animation */
	UPROPERTY(VisibleDefaultsOnly, Category = Damage)
	UWidgetComponent* FloatingTextComponent;

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	USkeletalMeshComponent* Mesh1P;

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* FP_Gun;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USceneComponent* FP_MuzzleLocation;

	/** Gun mesh: VR view (attached to the VR controller directly, no arm, just the actual gun) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* VR_Gun;

	/** Location on VR gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USceneComponent* VR_MuzzleLocation;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;

	/** Motion controller (right hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UMotionControllerComponent* R_MotionController;

	/** Motion controller (left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UMotionControllerComponent* L_MotionController;

public:
	AFirstPersonShooterCharacter(const FObjectInitializer& ObjectInitializer);

	/** Responsible for damaging self */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Damage)
	void OnDamage(float Amount, AActor* DamageSource);
	virtual void OnDamage_Implementation(float Amount, AActor* DamageSource) override;

	/** Responsible for Handling changes in health */
	UFUNCTION()
	void OnRep_Health();

	void PossessSpectator();

	UFUNCTION(Server, Reliable)
	void ServerPossessSpectator();

	UFUNCTION()
	void TriggerSuperSkill();

	UFUNCTION()
	void TriggerNormalSkill();

	UFUNCTION()
	void StartSprinting();

	UFUNCTION()
	void StopSprinting();

	UFUNCTION()
	void StartSlow(float Multiplier);

	UFUNCTION()
	void StopSlow(float Multiplier);

	UFUNCTION(Server, Reliable)
	void ServerSetSlow(bool IsSlowed, float Multiplier);

protected:
	virtual void BeginPlay();
	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const override;
public:
	/** Character's Sprinting flag */
	UPROPERTY(Replicated)
	float SlowMultiplier = 1.0f;


	/** Character's Sprinting flag */
	UPROPERTY(Replicated)
	bool bIsSprinting;

	/** Character's Slow flag */
	UPROPERTY(Replicated)
	bool bIsSlowed;

	/** Character's Health */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Stats)
	float MaxHealth;

	UPROPERTY(ReplicatedUsing=OnRep_Health)
	float CurrentHealth;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health)
	bool bIsDead;

	/** Current Rotation that will be synced to all clients */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = General)
	FRotator CurrentRotation;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector GunOffset;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category=Projectile)
	TSubclassOf<class AFirstPersonShooterProjectile> ProjectileClass;

	/** UserWidget class to use for Floating Text */
	UPROPERTY(EditDefaultsOnly, Category = General)
	TSubclassOf<class UUserWidget> FloatingTextClass;

	/** Spectator Pawn Class To Spawn upon death */
	UPROPERTY(EditDefaultsOnly, Category = General)
	TSubclassOf<class ASpectatorPawn> SpectatorPawnClass;

	/** SuperSkill to be attached */
	UPROPERTY(EditDefaultsOnly, Category = General)
	TSubclassOf<UCharacterSuperSkill> CharacterSuperSkillClass;

	/** NormalSkill to be attached */
	UPROPERTY(EditDefaultsOnly, Category = General)
	TSubclassOf<UCharacterNormalSkill> CharacterNormalSkillClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	UAnimMontage* FireAnimation;

	/** Whether to use motion controller location for aiming. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	uint8 bUsingMotionControllers : 1;

protected:
	/** Handles Deducting of health on the server */
	UFUNCTION(Server, Reliable)
	void ServerDeductHealth(float Amount, AActor* DamageSource);

	/** Handles Triggering of Damage Popup on the damageSource */
	UFUNCTION(Client, Reliable)
	void ClientTriggerPopup(AActor* TargetEnemy, float Amount);

	/** Handles Spawning of Projectile Overall */
	void FireProjectile();

	/** Handles Spawning of Projectile Locally */
	void FireProjectileLocal();

	/** Server RPC for handling Projectile Spawning */
	UFUNCTION(Server, Reliable)
	void ServerSpawnProjectile();

	/** Multicast RPC for handling Projectile Spawning */
	UFUNCTION(NetMulticast, Reliable)
	void MulticastSpawnProjectile();

	/** Server RPC for handling sycing of rotation */
	UFUNCTION(Server, Reliable)
	void ServerSyncControlRotation(const FRotator& PlayerRotation);

	/** Multicast RPC for handling sycing of rotation */
	UFUNCTION(NetMulticast, Reliable)
	void MulticastSyncControlRotation(const FRotator& PlayerRotation);

	/** Fires a projectile. */
	UFUNCTION(BlueprintCallable)
	void OnFire();

	/** Resets HMD orientation and position in VR. */
	void OnResetVR();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAxis(float Rate);

	struct TouchData
	{
		TouchData() { bIsPressed = false;Location=FVector::ZeroVector;}
		bool bIsPressed;
		ETouchIndex::Type FingerIndex;
		FVector Location;
		bool bMoved;
	};
	void BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location);
	TouchData	TouchItem;
	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

	/* 
	 * Configures input for touchscreen devices if there is a valid touch interface for doing so 
	 *
	 * @param	InputComponent	The input component pointer to bind controls to
	 * @returns true if touch controls were enabled.
	 */
	bool EnableTouchscreenMovement(UInputComponent* InputComponent);

public:
	/** Returns Mesh1P subobject **/
	USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

};

