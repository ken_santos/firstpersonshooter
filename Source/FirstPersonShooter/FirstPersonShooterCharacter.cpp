// Copyright Epic Games, Inc. All Rights Reserved.

#include "FirstPersonShooterCharacter.h"
#include "FirstPersonShooterProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId
#include "Net/UnrealNetwork.h"
#include "Engine/Engine.h"
#include "Components/WidgetComponent.h"
#include "FloatingTextWidget.h"
#include "GameFramework/SpectatorPawn.h"
#include "GameFramework/PlayerController.h"
#include "FirstPersonGameState.h"
#include "Engine/World.h"
#include "CharacterSuperSkill.h"
#include "CharacterNormalSkill.h"
#include "ShooterCharacterMovement.h"
#include "FirstPersonShooterMovement.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AFirstPersonShooterCharacter

AFirstPersonShooterCharacter::AFirstPersonShooterCharacter(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer.SetDefaultSubobjectClass<UFirstPersonShooterMovement>(ACharacter::CharacterMovementComponentName))
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(false);			// otherwise won't be visible in the multiplayer
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->MotionSource = FXRMotionControllerBase::RightHandSourceId;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(false);			// otherwise won't be visible in the multiplayer
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.

	// Uncomment the following line to turn motion controllers on by default:
	// bUsingMotionControllers = true;

	FloatingTextComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("FloatingTextComponent"));
	FloatingTextComponent->SetupAttachment(GetCapsuleComponent());

	bIsDead = false;
	bIsSprinting = false;
	bIsSlowed = false;
}

void AFirstPersonShooterCharacter::OnDamage_Implementation(float Amount, AActor* DamageSource)
{
	ServerDeductHealth(Amount, DamageSource);
}


void AFirstPersonShooterCharacter::OnRep_Health()
{
	if (CurrentHealth <= 0.0f) {
		bIsDead = true;
	}
}

void AFirstPersonShooterCharacter::PossessSpectator()
{
	ServerPossessSpectator();
}

void AFirstPersonShooterCharacter::ServerPossessSpectator_Implementation()
{
	if (SpectatorPawnClass) {
		UWorld* const World = GetWorld();
		if (World)
		{
			const FRotator SpawnRotation = GetBaseAimRotation();
			const FVector SpawnLocation = GetActorLocation();

			ASpectatorPawn* spawnedSpectator = World->SpawnActor<ASpectatorPawn>(SpectatorPawnClass, SpawnLocation, SpawnRotation);

			if (spawnedSpectator) {
				APlayerController* controller = Cast<APlayerController>(GetOwner());
				controller->Possess(spawnedSpectator);

				Destroy();
			}
		}
	}
}

void AFirstPersonShooterCharacter::TriggerSuperSkill()
{
	CharacterSuperSkillComponent->ActivateSkill();
}

void AFirstPersonShooterCharacter::TriggerNormalSkill()
{
	CharacterNormalSkillComponent->ActivateSkill();
}

void AFirstPersonShooterCharacter::StartSprinting()
{
	UFirstPersonShooterMovement* MoveComp = Cast<UFirstPersonShooterMovement>(GetCharacterMovement());
	if (MoveComp)
	{
		MoveComp->SetSprinting(true);
	}
}

void AFirstPersonShooterCharacter::StopSprinting()
{
	UFirstPersonShooterMovement* MoveComp = Cast<UFirstPersonShooterMovement>(GetCharacterMovement());
	if (MoveComp)
	{
		MoveComp->SetSprinting(false);
	}
}

void AFirstPersonShooterCharacter::StartSlow(float Multiplier)
{
}

void AFirstPersonShooterCharacter::StopSlow(float Multiplier)
{
}

void AFirstPersonShooterCharacter::ServerSetSlow_Implementation(bool IsSlowed, float Multiplier)
{
	bIsSlowed = IsSlowed;
	SlowMultiplier = Multiplier;
}

void AFirstPersonShooterCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	if (CharacterSuperSkillClass)
	{
		FName CharacterSuperName("CharacterSuperSkillComponent");
		CharacterSuperSkillComponent = NewObject<UCharacterSuperSkill>(this, CharacterSuperSkillClass, CharacterSuperName);

		if (CharacterSuperSkillComponent) {
			CharacterSuperSkillComponent->RegisterComponent();
		}
	}

	if (CharacterNormalSkillClass)
	{
		FName CharacterNormalName("CharacterNormalSkillComponent");
		CharacterNormalSkillComponent = NewObject<UCharacterNormalSkill>(this, CharacterNormalSkillClass, CharacterNormalName);

		if (CharacterNormalSkillComponent) {
			CharacterNormalSkillComponent->RegisterComponent();
		}
	}
	
	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}

	CurrentHealth = MaxHealth;
}

void AFirstPersonShooterCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AFirstPersonShooterCharacter, CurrentHealth);
}

//////////////////////////////////////////////////////////////////////////
// Input

void AFirstPersonShooterCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	//PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AFirstPersonShooterCharacter::OnFire);

	// Enable touchscreen input
	EnableTouchscreenMovement(PlayerInputComponent);

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AFirstPersonShooterCharacter::OnResetVR);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AFirstPersonShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFirstPersonShooterCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AFirstPersonShooterCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &AFirstPersonShooterCharacter::LookUpAxis);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AFirstPersonShooterCharacter::LookUpAtRate);

	// Bind Burn Skill events
	PlayerInputComponent->BindAction("SuperSkill", IE_Pressed, this, &AFirstPersonShooterCharacter::TriggerSuperSkill);

	PlayerInputComponent->BindAction("NormalSkill", IE_Pressed, this, &AFirstPersonShooterCharacter::TriggerNormalSkill);

	InputComponent->BindAction("Sprint", IE_Pressed, this, &AFirstPersonShooterCharacter::StartSprinting);
	InputComponent->BindAction("Sprint", IE_Released, this, &AFirstPersonShooterCharacter::StopSprinting);
}

void AFirstPersonShooterCharacter::ServerSpawnProjectile_Implementation()
{
	MulticastSpawnProjectile();
}

void AFirstPersonShooterCharacter::ServerSyncControlRotation_Implementation(const FRotator& PlayerRotation)
{
	MulticastSyncControlRotation(PlayerRotation);
}

void AFirstPersonShooterCharacter::MulticastSyncControlRotation_Implementation(const FRotator& PlayerRotation)
{
	CurrentRotation = PlayerRotation;
}

void AFirstPersonShooterCharacter::ServerDeductHealth_Implementation(float Amount, AActor* DamageSource)
{
	CurrentHealth -= Amount;
	if(HasAuthority())
		OnRep_Health();	

	if (bIsDead) {
		// Possess Spectator
		PossessSpectator();
	}
	else
	{
		AFirstPersonShooterCharacter* damageSource = Cast<AFirstPersonShooterCharacter>(DamageSource);

		damageSource->ClientTriggerPopup(this, Amount);
	}
}

void AFirstPersonShooterCharacter::ClientTriggerPopup_Implementation(AActor* TargetEnemy, float Amount)
{

	if (IsLocallyControlled())
	{
		AFirstPersonShooterCharacter* targetEnemy = Cast<AFirstPersonShooterCharacter>(TargetEnemy);

		Cast<UFloatingTextWidget>(targetEnemy->FloatingTextComponent->GetUserWidgetObject())->PlayFloatingText(Amount);
	}
}

void AFirstPersonShooterCharacter::FireProjectile()
{
	// Spawn Projectile On Server
	if (HasAuthority())
		MulticastSpawnProjectile();
	else
		ServerSpawnProjectile();

	// Fire The Projectile Locally
	FireProjectileLocal();
}

void AFirstPersonShooterCharacter::FireProjectileLocal()
{
	// try and fire a projectile
	if (ProjectileClass != nullptr)
	{
		UWorld* const World = GetWorld();
		if (World != nullptr)
		{
			if (bUsingMotionControllers)
			{
				const FRotator SpawnRotation = VR_MuzzleLocation->GetComponentRotation();
				const FVector SpawnLocation = VR_MuzzleLocation->GetComponentLocation();
				World->SpawnActor<AFirstPersonShooterProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
			}
			else
			{
				const FRotator SpawnRotation = GetBaseAimRotation();
				// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
				const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

				//Set Spawn Collision Handling Override
				FActorSpawnParameters ActorSpawnParams;
				ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

				// spawn the projectile at the muzzle
				AFirstPersonShooterProjectile* spawnedProjectile = World->SpawnActor<AFirstPersonShooterProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);

				spawnedProjectile->ProjectileOwner = this;
			}
		}
	}
}

void AFirstPersonShooterCharacter::MulticastSpawnProjectile_Implementation()
{
	if(!IsLocallyControlled())
		FireProjectileLocal();
}

void AFirstPersonShooterCharacter::OnFire()
{
	// Spawn Projectile
	FireProjectile();

	// try and play the sound if specified
	if (FireSound != nullptr)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	// try and play a firing animation if specified
	if (FireAnimation != nullptr)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != nullptr)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

void AFirstPersonShooterCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AFirstPersonShooterCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AFirstPersonShooterCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	TouchItem.bIsPressed = false;
}

//Commenting this section out to be consistent with FPS BP template.
//This allows the user to turn without using the right virtual joystick

//void AFirstPersonShooterCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
//{
//	if ((TouchItem.bIsPressed == true) && (TouchItem.FingerIndex == FingerIndex))
//	{
//		if (TouchItem.bIsPressed)
//		{
//			if (GetWorld() != nullptr)
//			{
//				UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
//				if (ViewportClient != nullptr)
//				{
//					FVector MoveDelta = Location - TouchItem.Location;
//					FVector2D ScreenSize;
//					ViewportClient->GetViewportSize(ScreenSize);
//					FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
//					if (FMath::Abs(ScaledDelta.X) >= 4.0 / ScreenSize.X)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.X * BaseTurnRate;
//						AddControllerYawInput(Value);
//					}
//					if (FMath::Abs(ScaledDelta.Y) >= 4.0 / ScreenSize.Y)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.Y * BaseTurnRate;
//						AddControllerPitchInput(Value);
//					}
//					TouchItem.Location = Location;
//				}
//				TouchItem.Location = Location;
//			}
//		}
//	}
//}

void AFirstPersonShooterCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AFirstPersonShooterCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AFirstPersonShooterCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AFirstPersonShooterCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AFirstPersonShooterCharacter::LookUpAxis(float Rate)
{
	AddControllerPitchInput(Rate);
	if (IsLocallyControlled()) {
		if (HasAuthority()) {
			MulticastSyncControlRotation(GetControlRotation());
		}
		else {
			ServerSyncControlRotation(GetControlRotation());
		}
	}
}

bool AFirstPersonShooterCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AFirstPersonShooterCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &AFirstPersonShooterCharacter::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AFirstPersonShooterCharacter::TouchUpdate);
		return true;
	}
	
	return false;
}
