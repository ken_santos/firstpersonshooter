// Fill out your copyright notice in the Description page of Project Settings.


#include "FloatingTextWidget.h"

#include "Components/TextBlock.h"
#include "Animation/WidgetAnimation.h"

void UFloatingTextWidget::NativeConstruct()
{
	Super::NativeConstruct();

    StoreWidgetAnimations();

    FloatingTextAnimation =  GetAnimationByName(TEXT("FloatingTextAnimation"));
}


void UFloatingTextWidget::PlayFloatingText(float Amount)
{
	int32 DamageAmount = FMath::RoundToInt(Amount);

	FloatingTextBlock->SetText(FText::AsNumber(DamageAmount));

    PlayAnimation(FloatingTextAnimation, 0.0f, 1, EUMGSequencePlayMode::Forward, 1.0f, false);
}

UWidgetAnimation* UFloatingTextWidget::GetAnimationByName(FName AnimationName) const
{
    UWidgetAnimation* const* WidgetAnimation = AnimationsMap.Find(AnimationName);
    if (WidgetAnimation)
    {
        return *WidgetAnimation;
    }
    return nullptr;
}

void UFloatingTextWidget::StoreWidgetAnimations()
{
    AnimationsMap.Empty();
    UProperty* Prop = GetClass()->PropertyLink;
    // check each property of this class
    while (Prop)
    {
        // only evaluate object properties, skip rest
        if (Prop->GetClass() == UObjectProperty::StaticClass())
        {
            UObjectProperty* ObjProp = Cast<UObjectProperty>(Prop);
            // only get back properties that are of type widget animation
            if (ObjProp->PropertyClass == UWidgetAnimation::StaticClass())
            {
                UObject* Obj = ObjProp->GetObjectPropertyValue_InContainer(this);
                // only get back properties that are of type widget animation
                UWidgetAnimation* WidgetAnimation = Cast<UWidgetAnimation>(Obj);
                // if casting worked update map with new animation
                if (WidgetAnimation && WidgetAnimation->MovieScene)
                {
                    FName AnimName = WidgetAnimation->MovieScene->GetFName();
                    AnimationsMap.Add(AnimName, WidgetAnimation);
                }
            }
        }
        Prop = Prop->PropertyLinkNext;
    }
}
