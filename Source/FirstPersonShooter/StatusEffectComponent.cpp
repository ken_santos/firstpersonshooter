// Fill out your copyright notice in the Description page of Project Settings.


#include "StatusEffectComponent.h"
#include "Engine/Engine.h"
#include "Kismet/KismetSystemLibrary.h"

void UStatusEffectComponent::BeginPlay()
{
	Super::BeginPlay();

	// Sets a timer that acts as the lifetime of the status effect component
	FTimerHandle Timer;
	GetOwner()->GetWorldTimerManager().SetTimer(Timer, this, &UStatusEffectComponent::DestroySelfAfterTimer, StatusEffectDuration);
}

void UStatusEffectComponent::ApplyStatusEffect()
{
}

void UStatusEffectComponent::SetDamageSource(AActor* EffectDamageSource)
{
	DamageSource = EffectDamageSource;
}

void UStatusEffectComponent::DestroySelfAfterTimer()
{
	DestroyComponent();
}
