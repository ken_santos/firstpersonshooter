// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacterMovement.h"
#include "FirstPersonShooterCharacter.h"
#include "ShooterCharacterMovement.h"


UShooterCharacterMovement::UShooterCharacterMovement(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}


float UShooterCharacterMovement::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	const AFirstPersonShooterCharacter* ShooterCharacterOwner = Cast<AFirstPersonShooterCharacter>(PawnOwner);
	if (ShooterCharacterOwner)
	{
		if (ShooterCharacterOwner->bIsSprinting)
		{
			MaxSpeed *= SprintSpeedModifier;
		}
		if (ShooterCharacterOwner->bIsSlowed)
		{
			MaxSpeed /= ShooterCharacterOwner->SlowMultiplier;
		}
	}

	return MaxSpeed;
}


//void UMyCharacterMovement::UpdateFromCompressedFlags(uint8 Flags)//Client only
//{
//	Super::UpdateFromCompressedFlags(Flags);
//
//  bWantsToSprint = (Flags&FSavedMove_Character::FLAG_Custom_0) != 0;
//}
//
//class FNetworkPredictionData_Client* UMyCharacterMovement::GetPredictionData_Client() const
//{
//	check(PawnOwner != NULL);
//	check(PawnOwner->Role < ROLE_Authority);
//
//	if (!ClientPredictionData)
//	{
//		UMyCharacterMovement* MutableThis = const_cast<UMyCharacterMovement*>(this);
//
//		MutableThis->ClientPredictionData = new FNetworkPredictionData_Client_MyMovement(*this);
//		MutableThis->ClientPredictionData->MaxSmoothNetUpdateDist = 92.f;
//		MutableThis->ClientPredictionData->NoSmoothNetUpdateDist = 140.f;
//	}
//
//	return ClientPredictionData;
//}
//
//void FSavedMove_MyMovement::Clear()
//{
//	Super::Clear();
//  bSavedWantsToSprint = false;
//
//}
//
//uint8 FSavedMove_MyMovement::GetCompressedFlags() const
//{
//	uint8 Result = Super::GetCompressedFlags();
//
//if (bSavedWantsToSprint)
//{
//	Result |= FLAG_Custom_0;
//}
//	return Result;
//}
//
//bool FSavedMove_MyMovement::CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* Character, float MaxDelta) const
//{
//
// if (bSavedWantsToSprint != ((FSavedMove_ExtendedMovement*)&NewMove)->bSavedWantsToSprint)
//{
//	return false;
//}
//	return Super::CanCombineWith(NewMove, Character, MaxDelta);
//}
//
//void FSavedMove_MyMovement::SetMoveFor(ACharacter* Character, float InDeltaTime, FVector const& NewAccel, class FNetworkPredictionData_Client_Character& ClientData)
//{
//	Super::SetMoveFor(Character, InDeltaTime, NewAccel, ClientData);
//
//	UMyCharacterMovement* CharMov = Cast<UMyCharacterMovement>(Character->GetCharacterMovement());
//	if (CharMov)
//	{
//	bSavedWantsToSprint = CharMov->bWantsToSprint
//	}
//}
//
//void FSavedMove_MyMovement::PrepMoveFor(class ACharacter* Character)
//{
//	Super::PrepMoveFor(Character);
//
//	UMyCharacterMovement* CharMov = Cast<UMyCharacterMovement>(Character->GetCharacterMovement());
//	if (CharMov)
//	{
//
//	}
//}
//
//FNetworkPredictionData_Client_MyMovement::FNetworkPredictionData_Client_MyMovement(const UCharacterMovementComponent& ClientMovement)
//	: Super(ClientMovement)
//{
//
//}
//
//FSavedMovePtr FNetworkPredictionData_Client_MyMovement::AllocateNewMove()
//{
//	return FSavedMovePtr(new FSavedMove_MyMovement());
//}